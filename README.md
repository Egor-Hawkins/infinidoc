Request Examples
```
POST http://localhost:8080/infinidoc/observations
Accept: application/json;charset=UTF-8
Content-Type: application/json;charset=UTF-8
Cache-Control: no-cache

{
  "detailDesc": "detailDesc1",
  "shortDesc": "shortDesc1",
  "realtyVersion": "1"
}

###

GET http://localhost:8080/infinidoc/observations
Accept: */*
Cache-Control: no-cache

###

POST http://localhost:8080/infinidoc/statistic
Accept: application/json;charset=UTF-8
Content-Type: application/json;charset=UTF-8
Cache-Control: no-cache

{
  "detailDesc": "detailDesc1",
  "shortDesc": "shortDesc1",
  "realtyVersion": "1",
  "observations": [
    {
      "id": 1,
      "shortDesc": "shortDesc1",
      "detailDesc": "detailDesc1",
      "created": "2019-06-09T16:12:11.16",
      "realtyVersion": 1
    }
  ]
}


###

GET http://localhost:8080/infinidoc/statistic
Accept: */*
Cache-Control: no-cache

###

POST http://localhost:8080/infinidoc/psychological
Accept: application/json;charset=UTF-8
Content-Type: application/json;charset=UTF-8
Cache-Control: no-cache

{
  "detailDesc": "detailDesc1",
  "shortDesc": "shortDesc1",
  "realtyVersion": "1",
  "observations": [
    {
      "id": 1,
      "shortDesc": "shortDesc1",
      "detailDesc": "detailDesc1",
      "created": "2019-06-09T16:12:11.16",
      "realtyVersion": 1
    }
  ],
  "statisticReports": [
    {
      "id": 2,
      "shortDesc": "shortDesc1",
      "detailDesc": "detailDesc1",
      "created": null,
      "realtyVersion": 1,
      "observations": [
        {
          "id": 1,
          "shortDesc": "shortDesc1",
          "detailDesc": "detailDesc1",
          "created": "2019-06-09T16:12:11.16",
          "realtyVersion": 1
        }
      ]
    }
  ]
}

###

GET http://localhost:8080/infinidoc/psychological
Accept: */*
Cache-Control: no-cache

###


POST http://localhost:8080/infinidoc/realty
Accept: application/json;charset=UTF-8
Content-Type: application/json;charset=UTF-8
Cache-Control: no-cache

{
  "detailDesc": "detailDesc1",
  "shortDesc": "shortDesc1",
  "realtyVersion": "1",
  "finished": "false",
  "observations": [
    {
      "id": 1,
      "shortDesc": "shortDesc1",
      "detailDesc": "detailDesc1",
      "created": "2019-06-09T16:12:11.16",
      "realtyVersion": 1
    }
  ],
  "psychologicalReports": [
    {
      "id": 3,
      "shortDesc": "shortDesc1",
      "detailDesc": "detailDesc1",
      "created": null,
      "realtyVersion": 1,
      "observations": [
        {
          "id": 1,
          "shortDesc": "shortDesc1",
          "detailDesc": "detailDesc1",
          "created": "2019-06-09T16:12:11.16",
          "realtyVersion": 1
        }
      ],
      "statisticReports": [
        {
          "id": 2,
          "shortDesc": "shortDesc1",
          "detailDesc": "detailDesc1",
          "created": null,
          "realtyVersion": 1,
          "observations": [
            {
              "id": 1,
              "shortDesc": "shortDesc1",
              "detailDesc": "detailDesc1",
              "created": "2019-06-09T16:12:11.16",
              "realtyVersion": 1
            }
          ]
        }
      ]
    }
  ],
  "assignedUser": {
    "userId": "first",
    "name": "name",
    "lastname": "lastname",
    "type": "technician"
  }
}

###

GET http://localhost:8080/infinidoc/realty
Accept: */*
Cache-Control: no-cache

###

POST http://localhost:9090/infinidoc/login
Content-Type: application/x-www-form-urlencoded

username=psychologist&password=psychologist

###

POST http://localhost:9090/infinidoc/logout

```

statistic statistic

observer observer

psychologist psychologist

sociologist sociologist

numerator numerator

technician technician
















