package ru.hawkins.infinidoc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.hawkins.infinidoc.dto.FilterDto;
import ru.hawkins.infinidoc.entities.ObservationReportEntity;
import ru.hawkins.infinidoc.repositories.ObservationReportRepository;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static ru.hawkins.infinidoc.repositories.FilterSpecifications.byFilter;

@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource(locations="classpath:application-test.properties")
public class ObservationReportIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ObservationReportRepository repository;

    @Test
    public void shouldSaveAndThenFindByFilter() {
        // given
        ObservationReportEntity observation = new ObservationReportEntity();
        observation.setShortDesc("shortDesc");
        entityManager.persist(observation);
        entityManager.flush();

        // when
        FilterDto filter = new FilterDto();
        filter.setShortDesc("shortDesc");
        List<ObservationReportEntity> found = repository.findAll(byFilter(filter));

        // then
        assertEquals(1, found.size());
        assertEquals("shortDesc", found.get(0).getShortDesc());
    }

}