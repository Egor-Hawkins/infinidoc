package ru.hawkins.infinidoc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.hawkins.infinidoc.dto.FilterDto;
import ru.hawkins.infinidoc.entities.ObservationReportEntity;
import ru.hawkins.infinidoc.entities.PsychologicalReportEntity;
import ru.hawkins.infinidoc.entities.StatisticReportEntity;
import ru.hawkins.infinidoc.repositories.PsychologicalReportRepository;
import ru.hawkins.infinidoc.repositories.StatisticReportRepository;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static ru.hawkins.infinidoc.repositories.FilterSpecifications.byFilter;

@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource(locations="classpath:application-test.properties")
public class PsychologicalReportIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PsychologicalReportRepository repository;

    @Test
    public void shouldSaveAndThenFindByFilter() {
        // given
        PsychologicalReportEntity entity = new PsychologicalReportEntity();
        entity.setShortDesc("shortDesc");
        entityManager.persist(entity);
        entityManager.flush();

        // when
        FilterDto filter = new FilterDto();
        filter.setShortDesc("shortDesc");
        List<PsychologicalReportEntity> found = repository.findAll(byFilter(filter));

        // then
        assertEquals(1, found.size());
        assertEquals(entity.getShortDesc(), found.get(0).getShortDesc());
    }

}