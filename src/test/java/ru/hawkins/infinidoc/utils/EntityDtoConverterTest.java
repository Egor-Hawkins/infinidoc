package ru.hawkins.infinidoc.utils;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.Test;

import ru.hawkins.infinidoc.dto.ObservationReportDto;
import ru.hawkins.infinidoc.entities.ObservationReportEntity;

public class EntityDtoConverterTest {

	@Test
	public void observationDtoToEntity() {
		ObservationReportDto observationReportDto = new ObservationReportDto();
		observationReportDto.setId(1);
		observationReportDto.setCreated(LocalDateTime.MAX);

		ObservationReportEntity observationReportEntity = EntityDtoConverter.observationDtoToEntity(observationReportDto);

		assertEquals(observationReportDto.getId(), observationReportEntity.getId());
		assertEquals(observationReportDto.getCreated(), observationReportEntity.getCreated());
	}

	@Test
	public void observationEntityToDto() {
		ObservationReportEntity observationReportEntity = new ObservationReportEntity();
		observationReportEntity.setId(1);
		observationReportEntity.setCreated(LocalDateTime.MAX);

		ObservationReportDto observationReportDto = EntityDtoConverter.observationEntityToDto(observationReportEntity);

		assertEquals(observationReportEntity.getId(), observationReportDto.getId());
		assertEquals(observationReportEntity.getCreated(), observationReportDto.getCreated());
	}
}