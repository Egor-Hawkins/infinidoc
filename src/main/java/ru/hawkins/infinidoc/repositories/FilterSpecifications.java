package ru.hawkins.infinidoc.repositories;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;
import ru.hawkins.infinidoc.dto.FilterDto;
import ru.hawkins.infinidoc.dto.RealtyChangeFilterDto;
import ru.hawkins.infinidoc.dto.UserFilter;
import ru.hawkins.infinidoc.entities.ObservationReportEntity;

import javax.persistence.criteria.Predicate;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class FilterSpecifications {
    public static <T> Specification<T> byFilter(FilterDto filter) {
        return (Specification<T>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (!StringUtils.isEmpty(filter.getShortDesc())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("shortDesc")), "%" + filter.getShortDesc().toUpperCase() + "%"));
            }

            if (!StringUtils.isEmpty(filter.getFullDesc())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("fullDesc")), "%" + filter.getFullDesc().toUpperCase() + "%"));
            }

            if (filter.getRealtyVersion() != null) {
                predicates.add(criteriaBuilder.equal(root.get("realtyVersion"), filter.getRealtyVersion()));
            }

            if (filter.getCreatedAfter() != null) {
                LocalDateTime triggerTime =
                        LocalDateTime.ofInstant(Instant.ofEpochMilli(filter.getCreatedAfter()), TimeZone.getDefault().toZoneId());

                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("created"), triggerTime));
            }

            if (filter.getCreatedBefore() != null) {
                LocalDateTime triggerTime =
                        LocalDateTime.ofInstant(Instant.ofEpochMilli(filter.getCreatedBefore()), TimeZone.getDefault().toZoneId());
                predicates.add(criteriaBuilder.lessThan(root.get("created"), triggerTime));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[] {}));
        };
    }

    public static <T> Specification<T> byFilter(RealtyChangeFilterDto filter) {
        return (Specification<T>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (!StringUtils.isEmpty(filter.getShortDesc())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("shortDesc")), "%" + filter.getShortDesc().toUpperCase() + "%"));
            }

            if (!StringUtils.isEmpty(filter.getFullDesc())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("fullDesc")), "%" + filter.getFullDesc().toUpperCase() + "%"));
            }

            if (filter.getRealtyVersion() != null) {
                predicates.add(criteriaBuilder.equal(root.get("realtyVersion"), filter.getRealtyVersion()));
            }

            if (filter.getCreatedAfter() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("created"), filter.getCreatedAfter()));
            }

            if (filter.getCreatedBefore() != null) {
                predicates.add(criteriaBuilder.lessThan(root.get("created"), filter.getCreatedBefore()));
            }

            if (!StringUtils.isEmpty(filter.getAssigned())) {
                predicates.add(criteriaBuilder.equal(root.get("assignedUser"), filter.getAssigned()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[] {}));
        };
    }

    public static <T> Specification<T> byFilter(UserFilter filter) {
        return (Specification<T>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (filter.getType() != null) {
                predicates.add(criteriaBuilder.equal(criteriaBuilder.upper(root.get("type")), filter.getType()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[] {}));
        };
    }
}
