package ru.hawkins.infinidoc.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.hawkins.infinidoc.entities.StatisticReportEntity;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
@Repository
public interface StatisticReportRepository extends CrudRepository<StatisticReportEntity, Integer>, JpaSpecificationExecutor<StatisticReportEntity> {
}
