package ru.hawkins.infinidoc.utils;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import ru.hawkins.infinidoc.dto.ObservationReportDto;
import ru.hawkins.infinidoc.dto.PsychologicalReportDto;
import ru.hawkins.infinidoc.dto.RealtyChangeProjectDto;
import ru.hawkins.infinidoc.dto.StatisticReportDto;
import ru.hawkins.infinidoc.dto.UserDto;
import ru.hawkins.infinidoc.entities.ObservationReportEntity;
import ru.hawkins.infinidoc.entities.PsychologicalReportEntity;
import ru.hawkins.infinidoc.entities.RealtyChangeProjectEntity;
import ru.hawkins.infinidoc.entities.StatisticReportEntity;
import ru.hawkins.infinidoc.entities.UserEntity;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
public class EntityDtoConverter {

	public static ObservationReportEntity observationDtoToEntity(ObservationReportDto dto) {
		ObservationReportEntity entity = new ObservationReportEntity();

		entity.setId(dto.getId());
		entity.setCreated(dto.getCreated());
		entity.setShortDesc(dto.getShortDesc());
		entity.setDetailDesc(dto.getDetailDesc());
		entity.setRealtyVersion(dto.getRealtyVersion());

		return entity;
	}

	public static ObservationReportDto observationEntityToDto(ObservationReportEntity entity) {
		ObservationReportDto dto = new ObservationReportDto();

		dto.setId(entity.getId());
		dto.setCreated(entity.getCreated());
		dto.setShortDesc(entity.getShortDesc());
		dto.setDetailDesc(entity.getDetailDesc());
		dto.setRealtyVersion(entity.getRealtyVersion());

		return dto;
	}

	public static StatisticReportEntity statisticReportDtoToEntity(StatisticReportDto dto) {
		StatisticReportEntity entity = new StatisticReportEntity();

		entity.setId(dto.getId());
		entity.setCreated(dto.getCreated());
		entity.setShortDesc(dto.getShortDesc());
		entity.setDetailDesc(dto.getDetailDesc());
		entity.setRealtyVersion(dto.getRealtyVersion());
		if (dto.getObservations() != null) {
			entity.setObservations(dto.getObservations().stream().map(EntityDtoConverter::observationDtoToEntity).collect(Collectors.toList()));
		}

		return entity;
	}

	public static StatisticReportDto statisticReportEntityToDto(StatisticReportEntity entity) {
		StatisticReportDto dto = new StatisticReportDto();

		dto.setId(entity.getId());
		dto.setCreated(entity.getCreated());
		dto.setShortDesc(entity.getShortDesc());
		dto.setDetailDesc(entity.getDetailDesc());
		dto.setRealtyVersion(entity.getRealtyVersion());
		if (dto.getObservations() != null) {
			dto.setObservations(entity.getObservations().stream().map(EntityDtoConverter::observationEntityToDto).collect(Collectors.toList()));
		}

		return dto;
	}

	public static PsychologicalReportEntity psychologicalReportDtoToEntity(PsychologicalReportDto dto) {
		PsychologicalReportEntity entity = new PsychologicalReportEntity();

		entity.setId(dto.getId());
		entity.setCreated(dto.getCreated());
		entity.setShortDesc(dto.getShortDesc());
		entity.setDetailDesc(dto.getDetailDesc());
		entity.setRealtyVersion(dto.getRealtyVersion());
		if (dto.getObservations() != null) {
			entity.setObservations(dto.getObservations().stream().map(EntityDtoConverter::observationDtoToEntity).collect(Collectors.toList()));
		}
		if (dto.getStatisticReports() != null) {
			entity.setStatisticReports(dto.getStatisticReports().stream().map(EntityDtoConverter::statisticReportDtoToEntity).collect(Collectors.toList()));
		}

		return entity;
	}

	public static PsychologicalReportDto psychologicalReportEntityToDto(PsychologicalReportEntity entity) {
		PsychologicalReportDto dto = new PsychologicalReportDto();

		dto.setId(entity.getId());
		dto.setCreated(entity.getCreated());
		dto.setShortDesc(entity.getShortDesc());
		dto.setDetailDesc(entity.getDetailDesc());
		dto.setRealtyVersion(entity.getRealtyVersion());
		if (dto.getObservations() != null) {
			dto.setObservations(entity.getObservations().stream().map(EntityDtoConverter::observationEntityToDto).collect(Collectors.toList()));
		}
		if (dto.getStatisticReports() != null) {
			dto.setStatisticReports(entity.getStatisticReports().stream().map(EntityDtoConverter::statisticReportEntityToDto).collect(Collectors.toList()));
		}

		return dto;
	}

	public static RealtyChangeProjectEntity realtyChangeProjectDtoToEntity(RealtyChangeProjectDto dto) {
		RealtyChangeProjectEntity entity = new RealtyChangeProjectEntity();

		entity.setId(dto.getId());
		entity.setCreated(dto.getCreated());
		entity.setShortDesc(dto.getShortDesc());
		entity.setDetailDesc(dto.getDetailDesc());
		entity.setRealtyVersion(dto.getRealtyVersion());
		entity.setFinished(dto.getFinished());
		entity.setAssignedUser(userDtoToEntity(dto.getAssignedUser()));
		if (dto.getObservations() != null) {
			entity.setObservations(dto.getObservations().stream().map(EntityDtoConverter::observationDtoToEntity).collect(Collectors.toList()));

		}
		if (dto.getPsychologicalReports() != null) {
			entity.setPsychologicalReports(dto.getPsychologicalReports().stream().map(EntityDtoConverter::psychologicalReportDtoToEntity).collect(Collectors.toList()));

		}

		return entity;
	}

	public static RealtyChangeProjectDto realtyChangeProjectEntityToDto(RealtyChangeProjectEntity entity) {
		RealtyChangeProjectDto dto = new RealtyChangeProjectDto();

		dto.setId(entity.getId());
		dto.setCreated(entity.getCreated());
		dto.setShortDesc(entity.getShortDesc());
		dto.setDetailDesc(entity.getDetailDesc());
		dto.setRealtyVersion(entity.getRealtyVersion());
		dto.setFinished(entity.getFinished());
		dto.setObservations(entity.getObservations().stream().map(EntityDtoConverter::observationEntityToDto).collect(Collectors.toList()));
		dto.setPsychologicalReports(entity.getPsychologicalReports().stream().map(EntityDtoConverter::psychologicalReportEntityToDto).collect(Collectors.toList()));
		dto.setAssignedUser(userEntityToDto(entity.getAssignedUser()));

		return dto;
	}

	public static UserEntity userDtoToEntity(UserDto dto) {
		UserEntity entity = new UserEntity();
		entity.setUserId(dto.getUserId());
		entity.setName(dto.getName());
		entity.setLastname(dto.getLastname());
		entity.setType(dto.getType());

		return entity;
	}

	public static UserDto userEntityToDto(UserEntity entity) {
		UserDto dto = new UserDto();
		dto.setUserId(entity.getUserId());
		dto.setName(entity.getName());
		dto.setLastname(entity.getLastname());
		dto.setType(entity.getType());

		return dto;
	}

}
