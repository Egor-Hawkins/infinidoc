package ru.hawkins.infinidoc.controllers;

import static ru.hawkins.infinidoc.repositories.FilterSpecifications.byFilter;
import static ru.hawkins.infinidoc.utils.EntityDtoConverter.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.hawkins.infinidoc.dto.FilterDto;
import ru.hawkins.infinidoc.dto.ObservationReportDto;
import ru.hawkins.infinidoc.dto.PsychologicalReportDto;
import ru.hawkins.infinidoc.entities.ObservationReportEntity;
import ru.hawkins.infinidoc.entities.PsychologicalReportEntity;
import ru.hawkins.infinidoc.entities.StatisticReportEntity;
import ru.hawkins.infinidoc.repositories.ObservationReportRepository;
import ru.hawkins.infinidoc.repositories.PsychologicalReportRepository;
import ru.hawkins.infinidoc.utils.EntityDtoConverter;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
@RestController
@RequestMapping(value = "psychological")
public class PsychologicalReportController {
	private PsychologicalReportRepository repository;
	private ObservationReportRepository observationRepository;

	@Autowired
	public PsychologicalReportController(PsychologicalReportRepository repository, ObservationReportRepository observationRepository) {
		this.repository = repository;
        this.observationRepository = observationRepository;
    }

	@GetMapping
	public ResponseEntity getAllPsychologicalReports(FilterDto filter) {
		List<PsychologicalReportDto> reports = new ArrayList<>();
        System.out.println("get psy by filter");
		repository.findAll(byFilter(filter)).forEach(e -> reports.add(psychologicalReportEntityToDto(e)));
		return ResponseEntity.ok(reports);
	}

    @GetMapping
    @RequestMapping("byObservations")
    public ResponseEntity getAllStatisticReportsByObservations(@RequestParam("ids[]") List<Integer> ids) {
        System.out.println("get psy by observations");
        return ResponseEntity.ok(
                observationRepository.findAllByIdIn(ids).stream()
                        .map(ObservationReportEntity::getPsychologicalReport)
                        .map(EntityDtoConverter::psychologicalReportEntityToDto)
                        .collect(Collectors.toList())
        );
    }

	@GetMapping
	@RequestMapping(value = "{id}/observations")
	ResponseEntity getLinkedObservations(@PathVariable Integer id, FilterDto filter) {
		return ResponseEntity.ok(repository.findById(id).orElseThrow(RuntimeException::new)
				.getObservations().stream()
				.map(EntityDtoConverter::observationEntityToDto)
				.collect(Collectors.toList()));
	}

	@GetMapping
	@RequestMapping(value = "{id}/statistics")
	ResponseEntity getLinkStatistisc(@PathVariable Integer id, FilterDto filter) {
		return ResponseEntity.ok(repository.findById(id).orElseThrow(RuntimeException::new)
				.getStatisticReports().stream()
				.map(EntityDtoConverter::statisticReportEntityToDto)
				.collect(Collectors.toList()));
	}


	@PostMapping
	public ResponseEntity createObservation(@RequestBody PsychologicalReportDto reportDto) {
        System.out.println("psy created");
		PsychologicalReportEntity psychReport = repository.save(psychologicalReportDtoToEntity(reportDto));
		List<ObservationReportEntity> observations = observationRepository.findAllByIdIn(
				reportDto.getObservations().stream()
						.map(ObservationReportDto::getId)
						.collect(Collectors.toList())
		);
		observations.forEach((obs) -> obs.setPsychologicalReport(psychReport));
		observationRepository.saveAll(observations);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
}
