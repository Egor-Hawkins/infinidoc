package ru.hawkins.infinidoc.controllers;

import static ru.hawkins.infinidoc.repositories.FilterSpecifications.byFilter;
import static ru.hawkins.infinidoc.utils.EntityDtoConverter.realtyChangeProjectDtoToEntity;
import static ru.hawkins.infinidoc.utils.EntityDtoConverter.realtyChangeProjectEntityToDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.hawkins.infinidoc.dto.FilterDto;
import ru.hawkins.infinidoc.dto.ObservationReportDto;
import ru.hawkins.infinidoc.dto.RealtyChangeProjectDto;
import ru.hawkins.infinidoc.entities.ObservationReportEntity;
import ru.hawkins.infinidoc.entities.RealtyChangeProjectEntity;
import ru.hawkins.infinidoc.repositories.ObservationReportRepository;
import ru.hawkins.infinidoc.repositories.RealtyChangeProjectRepository;
import ru.hawkins.infinidoc.utils.EntityDtoConverter;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
@RestController
@RequestMapping(value = "realty")
public class RealtyChangeProjectController {
	private RealtyChangeProjectRepository repository;
	private ObservationReportRepository observationRepository;

	@Autowired
	public RealtyChangeProjectController(RealtyChangeProjectRepository repository, ObservationReportRepository observationRepository) {
		this.repository = repository;
		this.observationRepository = observationRepository;
	}

	@GetMapping
	public ResponseEntity getAllRealtyChangeProjects(FilterDto filter) {
		System.out.println("get statistic by filter");
		List<RealtyChangeProjectDto> reports = new ArrayList<>();
		repository.findAll(byFilter(filter)).forEach(e -> reports.add(realtyChangeProjectEntityToDto(e)));
		return ResponseEntity.ok(reports);
	}

	@GetMapping
	@RequestMapping("byObservations")
	public ResponseEntity getAllStatisticReportsByObservations(@RequestParam("ids[]") List<Integer> ids) {
		System.out.println("get realty by observations");
		return ResponseEntity.ok(
				observationRepository.findAllByIdIn(ids).stream()
						.map(ObservationReportEntity::getRealtyChangeProject)
						.map(EntityDtoConverter::realtyChangeProjectEntityToDto)
						.collect(Collectors.toList())
		);
	}

	@GetMapping
	@RequestMapping("{id}")
	public ResponseEntity get(@PathVariable Integer id) {
		Optional<RealtyChangeProjectEntity> change = repository.findById(id);
		return change.isPresent()? ResponseEntity.ok(realtyChangeProjectEntityToDto(change.get())) :
								   ResponseEntity.notFound().build();
	}


	@PostMapping
	public ResponseEntity createRealtyChangeProject(@RequestBody RealtyChangeProjectDto reportDto) {
		System.out.println("realty created");
		RealtyChangeProjectEntity change = repository.save(realtyChangeProjectDtoToEntity(reportDto));
		List<ObservationReportEntity> observations = observationRepository.findAllByIdIn(
				reportDto.getObservations().stream()
						.map(ObservationReportDto::getId)
						.collect(Collectors.toList())
		);
		observations.forEach((obs) -> obs.setRealtyChangeProject(change));
		observationRepository.saveAll(observations);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PutMapping
    public ResponseEntity updateRealtyChangeProject(@RequestBody RealtyChangeProjectDto dto) {
		System.out.println("realty updated");
	    repository.save(realtyChangeProjectDtoToEntity(dto));
	    return ResponseEntity.status(HttpStatus.OK).build();
    }
}
