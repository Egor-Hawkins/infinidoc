package ru.hawkins.infinidoc.controllers;

import static ru.hawkins.infinidoc.repositories.FilterSpecifications.byFilter;
import static ru.hawkins.infinidoc.utils.EntityDtoConverter.observationDtoToEntity;
import static ru.hawkins.infinidoc.utils.EntityDtoConverter.observationEntityToDto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ru.hawkins.infinidoc.dto.FilterDto;
import ru.hawkins.infinidoc.dto.ObservationReportDto;
import ru.hawkins.infinidoc.repositories.ObservationReportRepository;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
@RestController
@RequestMapping(value = "observations")
public class ObservationReportController {
	private ObservationReportRepository repository;

	@Autowired
	public ObservationReportController(ObservationReportRepository repository) {
		this.repository = repository;
	}

	@PostMapping
	public ResponseEntity createObservation(@RequestBody ObservationReportDto reportDto) {
		System.out.println("Obserrvation created");
		repository.save(observationDtoToEntity(reportDto));
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@GetMapping
	public ResponseEntity getAllObservations(FilterDto filter) {
		System.out.println("get all observations");
		List<ObservationReportDto> observations = new ArrayList<>();
		repository.findAll(byFilter(filter)).forEach(e -> observations.add(observationEntityToDto(e)));
		return ResponseEntity.ok(observations);
	}
}
