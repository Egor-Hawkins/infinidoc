package ru.hawkins.infinidoc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.hawkins.infinidoc.dto.UserDto;
import ru.hawkins.infinidoc.dto.UserFilter;
import ru.hawkins.infinidoc.repositories.UsersRepository;

import java.util.ArrayList;
import java.util.List;

import static ru.hawkins.infinidoc.repositories.FilterSpecifications.byFilter;
import static ru.hawkins.infinidoc.utils.EntityDtoConverter.userEntityToDto;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
@RestController
@RequestMapping(value = "users")
public class UsersController {
	private UsersRepository repository;

	@Autowired
	public UsersController(UsersRepository repository) {
		this.repository = repository;
	}

	@GetMapping
	public ResponseEntity getUsers(UserFilter filter) {
		List<UserDto> observations = new ArrayList<>();
		repository.findAll(byFilter(filter)).forEach(e -> observations.add(userEntityToDto(e)));
		return ResponseEntity.ok(observations);
	}

	@GetMapping
	@RequestMapping("authorities")
	public ResponseEntity getCurrentUserAuthorities() {
		return ResponseEntity.ok(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
	}
}
