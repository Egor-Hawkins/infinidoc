package ru.hawkins.infinidoc.controllers;

import static ru.hawkins.infinidoc.repositories.FilterSpecifications.byFilter;
import static ru.hawkins.infinidoc.utils.EntityDtoConverter.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.hawkins.infinidoc.dto.FilterDto;
import ru.hawkins.infinidoc.dto.ObservationReportDto;
import ru.hawkins.infinidoc.dto.StatisticReportDto;
import ru.hawkins.infinidoc.entities.ObservationReportEntity;
import ru.hawkins.infinidoc.entities.StatisticReportEntity;
import ru.hawkins.infinidoc.repositories.ObservationReportRepository;
import ru.hawkins.infinidoc.repositories.StatisticReportRepository;
import ru.hawkins.infinidoc.utils.EntityDtoConverter;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
@RestController
@RequestMapping(value = "statistic")
public class StatisticReportController {
	private StatisticReportRepository repository;
	private ObservationReportRepository observationRepository;

	@Autowired
	public StatisticReportController(StatisticReportRepository repository, ObservationReportRepository observationRepository) {
		this.repository = repository;
		this.observationRepository = observationRepository;
	}

	@GetMapping
	public ResponseEntity getAllStatisticReports(FilterDto filter) {
		System.out.println("get statistic by filter");
		List<StatisticReportDto> reports = new ArrayList<>();
		repository.findAll(byFilter(filter)).forEach(e -> reports.add(statisticReportEntityToDto(e)));
		return ResponseEntity.ok(reports);
	}

	@GetMapping
	@RequestMapping("byObservations")
	public ResponseEntity getAllStatisticReportsByObservations(@RequestParam("ids[]") List<Integer> ids) {
		System.out.println("get statistic by observations");
		return ResponseEntity.ok(
				observationRepository.findAllByIdIn(ids).stream()
						.map(ObservationReportEntity::getStatisticReport)
						.map(EntityDtoConverter::statisticReportEntityToDto)
						.collect(Collectors.toList())
		);
	}

	@GetMapping
	@RequestMapping(value = "{id}/observations")
	ResponseEntity getLinkedObservations(@PathVariable Integer id, FilterDto filter) {
		return ResponseEntity.ok(repository.findById(id).orElseThrow(RuntimeException::new)
				.getObservations().stream()
				.map(EntityDtoConverter::observationEntityToDto)
				.collect(Collectors.toList()));
	}


	@PostMapping
	public ResponseEntity createStatisticReport(@RequestBody StatisticReportDto reportDto) {
		System.out.println("statistoc created");
		StatisticReportEntity statReport = repository.save(statisticReportDtoToEntity(reportDto));
		List<ObservationReportEntity> observations = observationRepository.findAllByIdIn(
				reportDto.getObservations().stream()
						.map(ObservationReportDto::getId)
						.collect(Collectors.toList())
		);
		observations.forEach((obs) -> obs.setStatisticReport(statReport));
		observationRepository.saveAll(observations);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
}
