package ru.hawkins.infinidoc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 22 Dec 2019
 */
@Configuration
@EnableWebSecurity
public class CustomWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
				.authorizeRequests()
				.antMatchers("/observations").authenticated()
				.antMatchers("/psychological").hasAnyAuthority("PSYCHOLOGIST")
				.antMatchers("/realty").hasAnyAuthority("OBSERVER", "TECHNICIAN", "NUMERATOR")
				.antMatchers("/statistic").hasAnyAuthority("STATISTIC", "PSYCHOLOGIST", "TECHNICIAN")
				.anyRequest().authenticated()
				.and()
				.formLogin()
				.defaultSuccessUrl("/")
				.and()
				.logout()
				.deleteCookies("JSESSIONID");
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
				.withUser("statistic").password(passwordEncoder().encode("statistic"))
				.authorities("STATISTIC")
				.and()
				.withUser("observer").password(passwordEncoder().encode("observer"))
				.authorities("OBSERVER")
				.and()
				.withUser("psychologist").password(passwordEncoder().encode("psychologist"))
				.authorities("PSYCHOLOGIST")
				.and()
				.withUser("sociologist").password(passwordEncoder().encode("sociologist"))
				.authorities("SOCIOLOGIST")
				.and()
				.withUser("numerator").password(passwordEncoder().encode("numerator"))
				.authorities("NUMERATOR")
				.and()
				.withUser("technician").password(passwordEncoder().encode("technician"))
				.authorities("TECHNICIAN");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
