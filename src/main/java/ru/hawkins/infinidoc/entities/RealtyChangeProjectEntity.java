package ru.hawkins.infinidoc.entities;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
@Entity
public class RealtyChangeProjectEntity {
	@Id
	@GeneratedValue
	private Integer id;

	@Column
	private String shortDesc;

	@Column
	private String detailDesc;

	@Column
	private LocalDateTime created;

	@Column
	private Integer realtyVersion;

	@Column
	private Boolean finished;

	@ManyToMany
	@JoinColumn(name="realty_change_id")
	private List<ObservationReportEntity> observations;

	@ManyToMany
	@JoinColumn(name = "id")
	@JoinColumn(name="realty_change_id")
	private List<PsychologicalReportEntity> psychologicalReports;

	@ManyToOne
	private UserEntity assignedUser;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getDetailDesc() {
		return detailDesc;
	}

	public void setDetailDesc(String detailDesc) {
		this.detailDesc = detailDesc;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public Integer getRealtyVersion() {
		return realtyVersion;
	}

	public void setRealtyVersion(Integer realtyVersion) {
		this.realtyVersion = realtyVersion;
	}

	public List<ObservationReportEntity> getObservations() {
		return observations;
	}

	public void setObservations(List<ObservationReportEntity> observations) {
		this.observations = observations;
	}

	public List<PsychologicalReportEntity> getPsychologicalReports() {
		return psychologicalReports;
	}

	public void setPsychologicalReports(List<PsychologicalReportEntity> psychologicalReports) {
		this.psychologicalReports = psychologicalReports;
	}

	public UserEntity getAssignedUser() {
		return assignedUser;
	}

	public void setAssignedUser(UserEntity assignedUser) {
		this.assignedUser = assignedUser;
	}

	public Boolean getFinished() {
		return finished;
	}

	public void setFinished(Boolean finished) {
		this.finished = finished;
	}
}
