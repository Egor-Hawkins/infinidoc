package ru.hawkins.infinidoc.entities;

import java.util.List;

import javax.persistence.*;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
@Entity
public class UserEntity {

	@Id
	private String userId;

	@Column
	private String name;

	@Column
	private String lastname;

	@Column
	@Enumerated(EnumType.STRING)
	private UserType type;

	@OneToMany
	private List<RealtyChangeProjectEntity> assignedProjects;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public UserType getType() {
		return type;
	}

	public void setType(UserType type) {
		this.type = type;
	}

	public List<RealtyChangeProjectEntity> getAssignedProjects() {
		return assignedProjects;
	}

	public void setAssignedProjects(List<RealtyChangeProjectEntity> assignedProjects) {
		this.assignedProjects = assignedProjects;
	}
}
