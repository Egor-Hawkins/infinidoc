package ru.hawkins.infinidoc.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
@Entity
public class ObservationReportEntity {
	@Id
	@GeneratedValue
	private Integer id;

	@Column
	private String shortDesc;

	@Column
	private String detailDesc;

	@Column
	private LocalDateTime created;

	@Column
	private Integer realtyVersion;

	@ManyToOne
	private StatisticReportEntity statisticReport;

	@ManyToOne
	private PsychologicalReportEntity psychologicalReport;

	@ManyToOne
	private RealtyChangeProjectEntity realtyChangeProject;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getDetailDesc() {
		return detailDesc;
	}

	public void setDetailDesc(String detailDesc) {
		this.detailDesc = detailDesc;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public Integer getRealtyVersion() {
		return realtyVersion;
	}

	public void setRealtyVersion(Integer realtyVersion) {
		this.realtyVersion = realtyVersion;
	}

	public StatisticReportEntity getStatisticReport() {
		return statisticReport;
	}

	public void setStatisticReport(StatisticReportEntity statisticReport) {
		this.statisticReport = statisticReport;
	}

	public PsychologicalReportEntity getPsychologicalReport() {
		return psychologicalReport;
	}

	public void setPsychologicalReport(PsychologicalReportEntity psychologicalReport) {
		this.psychologicalReport = psychologicalReport;
	}

	public RealtyChangeProjectEntity getRealtyChangeProject() {
		return realtyChangeProject;
	}

	public void setRealtyChangeProject(RealtyChangeProjectEntity realtyChangeProject) {
		this.realtyChangeProject = realtyChangeProject;
	}
}
