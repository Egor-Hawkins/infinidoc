package ru.hawkins.infinidoc.entities;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.*;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
@Entity
public class StatisticReportEntity {
	@Id
	@GeneratedValue
	private Integer id;

	@Column
	private String shortDesc;

	@Column
	private String detailDesc;

	@Column
	private LocalDateTime created;

	@Column
	private Integer realtyVersion;

	@OneToMany
	@JoinColumn(name="statistic_id")
	private List<ObservationReportEntity> observations;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getDetailDesc() {
		return detailDesc;
	}

	public void setDetailDesc(String detailDesc) {
		this.detailDesc = detailDesc;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public Integer getRealtyVersion() {
		return realtyVersion;
	}

	public void setRealtyVersion(Integer realtyVersion) {
		this.realtyVersion = realtyVersion;
	}

	public List<ObservationReportEntity> getObservations() {
		return observations;
	}

	public void setObservations(List<ObservationReportEntity> observations) {
		this.observations = observations;
	}
}
