package ru.hawkins.infinidoc.entities;

public enum UserType {
    PSYCHOLOGIST, OBSERVER, TECHNICIAN, STATISTIC
}
