package ru.hawkins.infinidoc.dto;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
public class PsychologicalReportDto {
	private Integer id;

	private String shortDesc;

	private String detailDesc;

	private LocalDateTime created;

	private Integer realtyVersion;

	private List<ObservationReportDto> observations;

	private List<StatisticReportDto> statisticReports;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getDetailDesc() {
		return detailDesc;
	}

	public void setDetailDesc(String detailDesc) {
		this.detailDesc = detailDesc;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public Integer getRealtyVersion() {
		return realtyVersion;
	}

	public void setRealtyVersion(Integer realtyVersion) {
		this.realtyVersion = realtyVersion;
	}

	public List<ObservationReportDto> getObservations() {
		return observations;
	}

	public void setObservations(List<ObservationReportDto> observations) {
		this.observations = observations;
	}

	public List<StatisticReportDto> getStatisticReports() {
		return statisticReports;
	}

	public void setStatisticReports(List<StatisticReportDto> statisticReports) {
		this.statisticReports = statisticReports;
	}
}
