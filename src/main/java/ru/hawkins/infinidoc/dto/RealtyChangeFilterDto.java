package ru.hawkins.infinidoc.dto;

public class RealtyChangeFilterDto extends FilterDto {
    private String assigned;

    public String getAssigned() {
        return assigned;
    }

    public void setAssigned(String assigned) {
        this.assigned = assigned;
    }
}
