package ru.hawkins.infinidoc.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

public class FilterDto {
    private String shortDesc;
    private String fullDesc;
    private String realtyVersion;
    private Long createdBefore;
    private Long createdAfter;
    private List<Integer> observationsIds;

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getFullDesc() {
        return fullDesc;
    }

    public void setFullDesc(String fullDesc) {
        this.fullDesc = fullDesc;
    }

    public String getRealtyVersion() {
        return realtyVersion;
    }

    public void setRealtyVersion(String realtyVersion) {
        this.realtyVersion = realtyVersion;
    }

    public List<Integer> getObservationsIds() {
        return observationsIds;
    }

    public void setObservationsIds(List<Integer> observationsIds) {
        this.observationsIds = observationsIds;
    }

    public Long getCreatedBefore() {
        return createdBefore;
    }

    public void setCreatedBefore(Long createdBefore) {
        this.createdBefore = createdBefore;
    }

    public Long getCreatedAfter() {
        return createdAfter;
    }

    public void setCreatedAfter(Long createdAfter) {
        this.createdAfter = createdAfter;
    }
}
