package ru.hawkins.infinidoc.dto;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author eyastrebov, <a href="mailto:Egor.Yastrebov@emergn.com">Egor Yastrebov</a>
 * @since 09 Jun 2019
 */
public class RealtyChangeProjectDto {
	private Integer id;

	private String shortDesc;

	private String detailDesc;

	private LocalDateTime created;

	private Integer realtyVersion;

	private Boolean finished;

	private List<ObservationReportDto> observations;

	private List<PsychologicalReportDto> psychologicalReports;

	private UserDto assignedUser;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getDetailDesc() {
		return detailDesc;
	}

	public void setDetailDesc(String detailDesc) {
		this.detailDesc = detailDesc;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public Integer getRealtyVersion() {
		return realtyVersion;
	}

	public void setRealtyVersion(Integer realtyVersion) {
		this.realtyVersion = realtyVersion;
	}

	public List<ObservationReportDto> getObservations() {
		return observations;
	}

	public void setObservations(List<ObservationReportDto> observations) {
		this.observations = observations;
	}

	public List<PsychologicalReportDto> getPsychologicalReports() {
		return psychologicalReports;
	}

	public void setPsychologicalReports(List<PsychologicalReportDto> psychologicalReports) {
		this.psychologicalReports = psychologicalReports;
	}

	public UserDto getAssignedUser() {
		return assignedUser;
	}

	public void setAssignedUser(UserDto assignedUser) {
		this.assignedUser = assignedUser;
	}

	public Boolean getFinished() {
		return finished;
	}

	public void setFinished(Boolean finished) {
		this.finished = finished;
	}
}
