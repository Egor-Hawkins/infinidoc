package ru.hawkins.infinidoc.dto;

import ru.hawkins.infinidoc.entities.UserType;

public class UserFilter {
    private UserType type;

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }
}
